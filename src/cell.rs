use std::cell::UnsafeCell;

pub struct Cell<T> {
    // Shared reference: can read from multiple places
    // Exclusive refrence: can write from single place
    // Only way to go from shared to excluvie in Rust: UnsafeCell
    value: UnsafeCell<T>,
}

impl<T> Cell<T> {
    pub fn new(value: T) -> Self {
        Cell {
            value: UnsafeCell::new(value),
        }
    }

    pub fn set(&self, value: T) {
        // UnsafeCell makes the set operation safe because it doesn't
        // implement Sync (!Sync): can't be shared across threads and thus
        // can't be modified without the user knowing
        unsafe {
            *self.value.get() = value;
        }
    }

    pub fn get(&self) -> T
    where
        T: Copy,
    {
        // Accessing the inner value is safe because UnsafeCell doesn't
        // implement Sync and the current thread is accessing this function
        // so the current value can't be modified
        // Returning copies is safe
        unsafe { *self.value.get() }
    }
}

#[cfg(test)]
mod test {
    use super::Cell;

    #[test]
    fn works() {
        let c = Cell::new("abc");
        let d = c.get();
        c.set("def");
        // Previous value copied
        assert_eq!(d, "abc");
        // New value set
        assert_eq!(c.get(), "def");
        // Getting returns the same
        assert_eq!(c.get(), "def");
    }
}
