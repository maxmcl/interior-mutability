use std::cell::{Cell, UnsafeCell};

#[derive(Copy, Clone)]
enum RefState {
    Unshared,
    Shared(usize),
    Exclusive,
}

// Borrow rules are checked at run time and not compile time
pub struct RefCell<T> {
    value: UnsafeCell<T>,
    state: Cell<RefState>,
}

impl<T> RefCell<T> {
    pub fn new(value: T) -> Self {
        Self {
            value: UnsafeCell::new(value),
            state: Cell::new(RefState::Unshared),
        }
    }

    pub fn borrow(&self) -> Option<Ref<'_, T>> {
        // Returns Some(&T) if no &mut has been handed out
        // get copies the value
        match self.state.get() {
            RefState::Unshared => {
                self.state.set(RefState::Shared(1));
                Some(Ref::new(self))
            }
            RefState::Shared(ref_count) => {
                self.state.set(RefState::Shared(ref_count + 1));
                Some(Ref::new(self))
            }
            RefState::Exclusive => None,
        }
    }

    pub fn borrow_mut(&self) -> Option<RefMut<'_, T>> {
        match self.state.get() {
            RefState::Unshared => {
                self.state.set(RefState::Exclusive);
                Some(RefMut::new(self))
            }
            _ => None,
        }
    }
}

pub struct Ref<'ref_cell, T> {
    // Wrapper around the Rust reference
    ref_cell: &'ref_cell RefCell<T>,
}

impl<'ref_cell, T> Ref<'ref_cell, T> {
    fn new(ref_cell: &'ref_cell RefCell<T>) -> Self {
        Self { ref_cell }
    }
}

impl<T> std::ops::Deref for Ref<'_, T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        unsafe { &*self.ref_cell.value.get() }
    }
}

impl<T> Drop for Ref<'_, T> {
    fn drop(&mut self) {
        // Ref is guaranteed to be returned only by a call to .borrow, which
        // means the ref state will be shared
        match self.ref_cell.state.get() {
            RefState::Shared(1) => {
                self.ref_cell.state.set(RefState::Unshared);
            }
            RefState::Shared(ref_count) => {
                self.ref_cell.state.set(RefState::Shared(ref_count - 1));
            }
            _ => unreachable!("Not possible"),
        }
    }
}

pub struct RefMut<'ref_cell, T> {
    // Wrapper around the Rust mutable reference
    ref_cell: &'ref_cell RefCell<T>,
}

impl<'ref_cell, T> RefMut<'ref_cell, T> {
    fn new(ref_cell: &'ref_cell RefCell<T>) -> Self {
        Self { ref_cell }
    }
}

impl<T> std::ops::Deref for RefMut<'_, T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        unsafe { &*self.ref_cell.value.get() }
    }
}

impl<T> std::ops::DerefMut for RefMut<'_, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { &mut *self.ref_cell.value.get() }
    }
}

impl<T> Drop for RefMut<'_, T> {
    fn drop(&mut self) {
        // RefMut is only returned by a call to borrow_mut, thus the
        // ref state must be exclusive
        if let RefState::Exclusive = self.ref_cell.state.get() {
            self.ref_cell.state.set(RefState::Unshared);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::RefCell;
    #[test]
    fn works() {}
}
